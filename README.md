*This is a demo app.*

### Installation

Install the dependencies and start the server.

```sh
$ npm install
$ npm start
```

### Todo
 - Create a .env file with the following format in the root dir.

```sh
# AWS
AWS_ACCESS_KEY=xxxxxxxx
AWS_SECRET_ACCESS_KEY=xxxxxxxx
BUCKET_NAME=xxxxxxxx
REGION=xxxxxxxx
CLOUDFRONT=xxxxxxxx // cloudfront or bucket url
```
$(document).ready(function () {
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none",
        beforeShow: function () {
            this.title = $(this.element).data("caption");
        }
    });
    $('#uploadForm').submit(function (e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: '/s3Upload',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                location.reload(true);
            }
        });
    });
    $('#editForm').submit(function (e) {
        var pid = $('#pid').val();
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: '/s3Update/' + pid,
            data: formData,
            processData: false,
            contentType: false,
            type: 'PUT',
            success: function (data) {
                location.reload(true);
            }
        });
    });
    $(".delete").click(function(){
        var photoid = $(this).attr('rel');
        if (confirm('Are you sure?')) {
            $.ajax({
                url : '/s3Delete/' + photoid,
                type: 'DELETE',
                success: function (data) {
                    location.reload(true);
                }
            });
        }
        
    });
});


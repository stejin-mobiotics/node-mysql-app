require('dotenv').config();
var express = require('express');
var router = express.Router();
var fs = require('fs');
let upload = require('../config/multer-config');
const awsWorker = require('../controllers/aws.controller');

/* GET home page. */
router.get('/', function (req, res, next) {

  var imageList = [];

  // Do the query to get data.
  db.query('SELECT * FROM images', function (err, rows, fields) {
    if (err) {
      res.status(500).json({ Error: err });
    } else {
      // Loop check on each row
      for (var i = 0; i < rows.length; i++) {

        // Create an object to save current row's data
        var image = {
          'photoid': rows[i].id,
          'FileName': rows[i].filename,
          'Caption': rows[i].caption
        }
        // Add object into array
        imageList.push(image);
      }

      // Render index.pug page using array 
      res.render('index', { "imageList": imageList, title: 'Gallery' });
    }
  });
});

// aws.controller
router.post('/s3Upload', upload.single("myFile"), awsWorker.doUpload);
router.delete('/s3Delete/:key', awsWorker.doDelete);
router.put('/s3Update/:id', upload.single("myFile"), awsWorker.doUpdate);

// UPLOAD
router.post('/upload', function (req, res, next) {
  var post = req.body;
  var caption = post.caption;
  if (!req.files)
    return res.status(400).json({ message: 'No files were uploaded.' });

  var file = req.files.myFile;
  var imgName = file.name;

  if (file.mimetype == "image/jpeg" || file.mimetype == "image/png" || file.mimetype == "image/gif") {
    file.mv('public/images/' + file.name, function (err) {
      var imgnamealt = Math.floor(100000 + Math.random() * 900000);
      var imgExtensions = imgName.split(".");
      fs.rename('public/images/' + file.name, 'public/images/' + imgnamealt + '.' + imgExtensions[1], function (err) {
        if (err) console.log('ERROR: ' + err);
      });
      if (err)
        return res.status(500).json({ message: err });
      var sqlInsert = "INSERT INTO `user`(`FileName`,`Caption`) VALUES ('" + imgnamealt + '.' + imgExtensions[1] + "','" + caption + "')";
      var query = db.query(sqlInsert, function (err, result) {
        if (err)
          return res.status(500).json({ message: err.sqlMessage });
        else
          return res.status(200).json({ message: 'Success' });
      });
    });
  } else {
    message = "This format is not allowed , please upload file with '.png','.gif','.jpg'";
    return res.status(415).json({ message: message });
  }
});

// DELETE
router.delete('/delete/:id', function (req, res) {
  let id = req.params.id;
  let sqlDelete = "SELECT * FROM `user` WHERE `photoid`='" + id + "'";

  db.query(sqlDelete, function (err, result) {
    if (err) return res.status(500).json({ message: err.sqlMessage });
    fs.unlink('public/images/' + result[0].FileName, function (err) {
      if (err) return res.status(500).json({ message: err });
    });
  });

  db.query('DELETE FROM `user` WHERE photoid="' + id + '"', function (err, result) {
    if (err)
      return res.status(500).json({ message: err });
    else
      return res.status(200).json({ message: "Success" });
  });
});

// UPDATE
router.put('/update/:id', function (req, res, next) {
  let id = req.params.id;
  var post = req.body;
  var caption = post.caption;
  var sqlUpdateCaption = "UPDATE `user` SET ";
  if (caption) {
    sqlUpdateCaption += "`Caption` = '" + caption + "'";
  }

  if (req.files) {
    var file = req.files.myFile, imgName = file.name;
    if (file.mimetype == "image/jpeg" || file.mimetype == "image/png" || file.mimetype == "image/gif") {
      var sqlImageToDelete = "SELECT `FileName` FROM `user` WHERE `photoid` = " + id;
      db.query(sqlImageToDelete, function (err, result) {
        if (err) return res.status(500).json({ message: err.sqlMessage });
        fs.unlink('public/images/' + result[0].FileName, function (err) {
          if (err) return res.status(500).json({ message: err });
        });
      });

      var imgnamealt = Math.floor(100000 + Math.random() * 900000);
      var imgExtensions = imgName.split(".");
      file.mv('public/images/' + file.name, function (err) {
        fs.rename('public/images/' + file.name, 'public/images/' + imgnamealt + '.' + imgExtensions[1], function (err) {
          if (err) console.log('ERROR: ' + err);
        });
        if (err)
          return res.status(500).json({ message: err });
      });
      sqlUpdateCaption += ", `FileName` = '" + imgnamealt + '.' + imgExtensions[1] + "'";
    } else {
      message = "This format is not allowed, please upload file with '.png','.gif','.jpg'";
      return res.status(415).json({ message: message });
    }
  }
  sqlUpdateCaption += " WHERE `photoid` = " + id;
  db.query(sqlUpdateCaption, function (err, result) {
    if (err)
      return res.status(500).json({ message: err.sqlMessage });
    else
      res.status(200).json({ message: 'Success' });
  });
});

router.get('/updateForm/:id', function (req, res, next) {
  var sql = 'SELECT * FROM images where id=' + req.params.id;
  db.query(sql, function (err, rows, fields) {
    if (err) {
      res.status(500).json({ "status_code": 500, "status_message": "internal server error" });
    } else {

      var imagedetails = {
        'photoid': rows[0].id,
        'FileName': rows[0].filename,
        'Caption': rows[0].caption
      }
      res.render('updateDetails', { imagedetails: imagedetails, title: 'Edit ' });
    }
  });
});

const sendEmail = () => {

  // Create sendEmail params 
  var params = {
    Destination: { /* required */
      ToAddresses: [
        'email@domain.com',
        /* more items */
      ]
    },
    Message: {
      Body: { /* required */
        Html: {
          Charset: "UTF-8",
          Data: "HTML_FORMAT_BODY"
        },
        Text: {
          Charset: "UTF-8",
          Data: "TEXT_FORMAT_BODY"
        }
      },
      Subject: {
        Charset: 'UTF-8',
        Data: 'Test email'
      }
    },
    Source: 'email@domain.com', /* required */
  };
  // Create the promise and SES service object
  var sendPromise = new AWS.SES({ apiVersion: '2010-12-01' }).sendEmail(params).promise();
  // Handle promise's fulfilled/rejected states
  sendPromise.then(
    function (data) {
      console.log(data.MessageId);
    }).catch(
      function (err) {
        console.error(err, err.stack);
      });

}

module.exports = router;

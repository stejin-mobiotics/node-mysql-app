const mysql = require('mysql');

var connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : 'root',
	database : ''
});

connection.connect(function (err) {
  if (err)
    console.error(err);
    return;
});

module.exports = connection;

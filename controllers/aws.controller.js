var stream = require('stream');
var path = require('path');

const s3 = require('../config/s3.config');

exports.doUpload = (req, res) => {
  var caption = req.body.caption
  if (!req.file)
    return res.status(400).json({ error: 'No files to upload.' });

  if (req.file.mimetype == "image/jpeg" ||
    req.file.mimetype == "image/png" ||
    req.file.mimetype == "image/gif") {
    const s3Client = s3.s3Client;
    const params = s3.uploadParams;
    // params.Key = req.file.originalname;
    var newName = Date.now() + path.extname(req.file.originalname)
    params.Key = newName
    params.Body = req.file.buffer;
    params.ContentType = req.file.mimetype;

    s3Client.upload(params, (err, data) => {
      if (err) {
        return res.status(500).json({ error: err });
      }
      var sqlInsert = "INSERT INTO `images`(`filename`,`caption`) VALUES ('" + newName + "','" + caption + "')";
      var query = db.query(sqlInsert, function (err, result) {
        if (err)
          return res.status(500).json({ error: err.sqlMessage });
        else
          return res.status(200).json({ message: 'Success -> Keyname = ' + newName });
      });
    });
  } else {
    message = "This format is not allowed , please upload file with '.png','.gif','.jpg'";
    return res.status(415).json({ error: message });
  }
}

exports.doUpdate = (req, res) => {
  let id = req.params.id;
  let caption = req.body.caption;
  let sqlUpdate = "UPDATE `images` SET ";
  if (caption) {
    sqlUpdate += "`caption` = '" + caption + "'";
  }

  if (req.file) {
    if (req.file.mimetype == "image/jpeg" ||
    req.file.mimetype == "image/png" ||
    req.file.mimetype == "image/gif") {
      var newName = Date.now() + path.extname(req.file.originalname)
      var sqlImageToDelete = "SELECT * FROM `images` WHERE `id` = " + id;
      db.query(sqlImageToDelete, function (err, result) {
        if (err) return res.status(500).json({ error: err.sqlMessage });
        const s3Client = s3.s3Client;
        var params = s3.deleteParams;
        params.Key = result[0].filename;

        s3Client.deleteObject(params, (err, data) => {
          if (err) return res.status(500).json({ error: err });
        });

        params = s3.uploadParams;
        params.Key = newName
        params.Body = req.file.buffer;
        params.ContentType = req.file.mimetype;

        s3Client.upload(params, (err, data) => {
          if (err) return res.status(500).json({ error: err });
        });
      });
      sqlUpdate += ", `filename` = '" + newName + "'";
    } else {
      message = "This format is not allowed , please upload file with '.png','.gif','.jpg'";
      return res.status(415).json({ message: message });
    }
  }

  sqlUpdate += " WHERE `id` = " + id;
  db.query(sqlUpdate, function (err, result) {
    if (err)
      return res.status(500).json({ error: err.sqlMessage });
    else
      return res.status(200).json({ message: 'Success' });
  });
}

exports.doDelete = (req, res) => {
  const s3Client = s3.s3Client;
  const params = s3.deleteParams;
  params.Key = req.params.key;

  s3Client.deleteObject(params, (err, data) => {
    if (err)
      return res.status(500).json({ error: err });
    db.query('DELETE FROM `images` WHERE filename="' + req.params.key + '"', function (err, result) {
      if (err)
        return res.status(500).json({ error: err });
      else
        return res.status(200).json({ message: "Success" });
    });
  });
}